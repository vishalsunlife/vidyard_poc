import { Component } from '@angular/core';
import VidyardEmbed from '@vidyard/embed-code';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {

    
    
  }

  ionViewDidEnter(){

    let playerUUID = 'aXbqNLoqBXWiCzn7qGe44b';
    let playerJWT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJhcGlfdG9rZW4iLCJleHAiOiIxNjI1NDAxMTQxIiwicm9sZV9pZCI6IjUyOTM5MiIsInBsYXllcl91dWlkIjoiYVhicU5Mb3FCWFdpQ3puN3FHZTQ0YiJ9.gqoukGfWzej9J6K8T7vWi0aOBg_wNHcVilzge3HphQs';
  
  

    window['onVidyardAPI'] = (vidyardEmbed) => {
      vidyardEmbed.api.renderPlayer({
        uuid: playerUUID,
        container: document.querySelector('.player'),
        type: 'inline',
        jwt: playerJWT
      });
    };


  
  }
}
